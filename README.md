# MLSpotifyAPI

Use of Machine Learning to recommend similar songs (unsupervised ML) and predict a song's popularity according to its features (supervised ML).
This is done thanks to Scikit-Learn and a [Spotify dataset](https://www.kaggle.com/yamaerenay/spotify-dataset-19212020-160k-tracks) on Kaggle.
